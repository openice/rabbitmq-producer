package com.example.spring;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.spring.controller.OrderSender;
import com.example.spring.entity.Order;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitmqProducerApplicationTests {

	@Test
	public void contextLoads() {
	}
	 @Autowired
	    private OrderSender orderSender;

	    @Test
	    public void testSend1() throws Exception {
	        Order order = new Order();
	        order.setId("20181001211111");
	        order.setName("测试订单1");
	        order.setMessageId(System.currentTimeMillis() + "$" + UUID.randomUUID().toString().replaceAll("-",""));
	        this.orderSender.send(order);
	    }
}
