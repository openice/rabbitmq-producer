package com.example.spring.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.example.spring.entity.Order;

@Controller
public class OrderSender {
	 private RabbitTemplate rabbitTemplate;

	    public OrderSender(
	            RabbitTemplate rabbitTemplate) {
	        this.rabbitTemplate = rabbitTemplate;
	    }
	 public void send(Order order) {
		    //exchange：交换机
	        // routingKey：路由键
	        // message：消息体内容
	        // correlationData：消息唯一ID
		 CorrelationData correlationData = new CorrelationData();
		 correlationData.setId(order.getMessageId());
		 this.rabbitTemplate.convertAndSend("exchange", "order.b", order, correlationData);
	 }
}
